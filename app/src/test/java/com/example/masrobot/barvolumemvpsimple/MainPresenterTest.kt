package com.example.masrobot.barvolumemvpsimple

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Before
import org.mockito.Mockito.mock



@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

    var presenter: MainPresenter? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        val view = mock(MainView::class.java)
        presenter = MainPresenter(view!!)
    }

    @Test
    @Throws(Exception::class)
    fun testVolumeWithIntegerInput() {
        val volume: Double = presenter?.volume(2.0, 8.0, 1.0)!!
    }
}