package com.example.masrobot.barvolumemvpsimple

class MainPresenter(val view: MainView) {

    fun volume(panjang: Double, lebar: Double, tinggi: Double): Double = panjang * lebar * tinggi

    fun hitungVolume(panjang: Double, lebar: Double, tinggi: Double) {
        val volume = volume(panjang, lebar, tinggi)
        val model = MainModel(volume)
        view.tampilVolume(model)
    }
}