package com.example.masrobot.barvolumemvpsimple

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val presenter = MainPresenter(this)
        btn_calculate.setOnClickListener {
            var length = et_panjang.text.toString().trim()
            var width = et_lebar.text.toString().trim()
            var height = et_tinggi.text.toString().trim()
            var isEmptyField = false

            if (TextUtils.isEmpty(length)) {
                isEmptyField = true
                et_panjang.error = "Field ini tidak boleh kosong"
            }

            if (TextUtils.isEmpty(width)) {
                isEmptyField = true
                et_lebar.error = "Field ini tidak boleh kosong"
            }

            if (TextUtils.isEmpty(height)) {
                isEmptyField = true
                et_tinggi.error = "Field ini tidak boleh kosong"
            }

            if (!isEmptyField) {
                val l = length.toDouble()
                val w = width.toDouble()
                val h = height.toDouble()

                presenter.hitungVolume(l, w, h)
            }
        }
    }

    override fun tampilVolume(model: MainModel) {
        tv_result.text = model.volume.toString()
    }
}
